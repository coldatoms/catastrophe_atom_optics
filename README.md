# catastrophe_atom_optics

[![Binder][BinderLogo]][BinderLink]

Visualization tools to accompany our paper "Catastrophe Atom Optics: Fold and Cusp
Caustics with an Atom Laser".  These can be run on [Binder] to interacting with the
visualizations by clicking the [![Binder][BinderLogo]][BinderLink] above.

Note: Several packages need to be installed on Binder so it may take a few (~10) minutes to
setup the environment the first time.

[Binder]: <https://mybinder.org/> "Binder"
[BinderLogo]: <https://mybinder.org/badge_logo.svg> "Launch Binder logo"
[BinderLink]: <https://mybinder.org/v2/gl/coldatoms%2Fcatastrophe_atom_optics/HEAD?filepath=CausticVisualization.ipynb> "Link to Binder"

The [`CausticVisualization.ipynb`](CausticVisualization.ipynb) notebook allows you to play with the code used to generate the following models:

* Two repulsive potentials: [Fig.1](https://viewer.pyvista.org/?fileURL=https://swan.physics.wsu.edu/Public/papers/Mossman_2021/fig1.vtkjs)
* Single repulsive Gaussian potential: [Fig.3a](https://viewer.pyvista.org/?fileURL=https://swan.physics.wsu.edu/Public/papers/Mossman_2021/fig3a.vtkjs), [Fig.3b](https://viewer.pyvista.org/?fileURL=https://swan.physics.wsu.edu/Public/papers/Mossman_2021/fig3b.vtkjs), [Fig.3c](https://viewer.pyvista.org/?fileURL=https://swan.physics.wsu.edu/Public/papers/Mossman_2021/fig3c.vtkjs), [Fig.3d](https://viewer.pyvista.org/?fileURL=https://swan.physics.wsu.edu/Public/papers/Mossman_2021/fig3d.vtkjs)
* Single attractive Gaussian potential: [Fig.3e](https://viewer.pyvista.org/?fileURL=https://swan.physics.wsu.edu/Public/papers/Mossman_2021/fig3e.vtkjs), [Fig.3f](https://viewer.pyvista.org/?fileURL=https://swan.physics.wsu.edu/Public/papers/Mossman_2021/fig3f.vtkjs), [Fig.3g](https://viewer.pyvista.org/?fileURL=https://swan.physics.wsu.edu/Public/papers/Mossman_2021/fig3g.vtkjs), [Fig.3h](https://viewer.pyvista.org/?fileURL=https://swan.physics.wsu.edu/Public/papers/Mossman_2021/fig3h.vtkjs).
